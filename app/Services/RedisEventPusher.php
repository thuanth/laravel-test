<?php
/**
 * Created by PhpStorm.
 * User: khanhdoan
 * Date: 5/16/2016
 * Time: 2:40 PM
 */

namespace App\Services;
use App\Contracts\EventPusher;

class RedisEventPusher implements EventPusher  {
    public function sentMailCustomer($email){
        return "Sent: ".$email;
    }
    public function cronJobSendMail(){
        return "Auto send mail";
    }
}