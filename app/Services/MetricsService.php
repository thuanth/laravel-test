<?php

namespace App\Services;


class MetricsService
{
    private $username;

    public function monthlyRevenue() {
        return date('m');
    }

    public function printCustomerName() {
        return "Doan Quoc Khanh";
    }

    public function getUsername() {
        return $this->username;
    }

    public function setUsername($username) {
        $this->username = $username;
    }
}
