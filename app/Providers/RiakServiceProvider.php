<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\MetricsService;

class RiakServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->printName();
        $this->printAge();
        $this->regEventPusher();
        $this->contextBinding();
    }

    public function printName() {
        $this->app->singleton('printMyName', function () {
            $metric = new MetricsService;
            return $metric;
        });
    }

    public function printAge() {
        $this->app->bind('printAge', function () {
            return new MetricsService;
        });
    }
    public function regEventPusher(){
        $this->app->bind('App\Contracts\EventPusher', 'App\Services\RedisEventPusher');
    }

    public function contextBinding() {
        $this->app->when('App\Http\PostSearchController')->needs('App\Contracts\SearchServiceInterface')->give('App\Services\PostSearchRepo');
        $this->app->when('App\Http\MemberSearchController')->needs('App\Contracts\SearchServiceInterface')->give('App\Services\MemberSearchRepo');
    }
}
