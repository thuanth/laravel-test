<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::group(['middleware' => ['web','auth.basic']], function () {
    Route::get('contact', [
        'as' => 'contact', 'uses' => 'ContactController@viewContact'
    ]);
    Route::post('contact', [
        'as' => 'post_contact', 'uses' => 'ContactController@postContact'
    ]);
});
Route::resource('photo', 'PhotoController');
Route::singularResourceParameters();
Route::resourceParameters([
    'photo' => 'image'
]);
Route::resource('member', 'MemberSearchController');
Route::resource('post_search', 'PostSearchController');
Route::auth();

Route::get('/home', [
    'middleware' => ['api','auth'],'as'=>'home',
    'uses' => 'HomeController@index'
]);
Route::get('profile', [
    'middleware' => 'auth:api',
    'uses' => 'ProfileController@show'
]);

Route::group(['middleware' => ['guest']], function () {
    //Login Routes...
    Route::get('/admin/login','AdminAuth\AuthController@showLoginForm');
    Route::post('/admin/login','AdminAuth\AuthController@login')->name('login');
    Route::get('/admin/logout','AdminAuth\AuthController@logout');

    // Registration Routes...
    Route::get('admin/register', 'AdminAuth\AuthController@showRegistrationForm');
    Route::post('admin/register', 'AdminAuth\AuthController@register');
    Route::get('admin/users/forgot', 'AdminAuth\PasswordController@getEmail');
    Route::post('admin/users/forgot', 'AdminAuth\PasswordController@postEmail');
    Route::get('/admin', 'AdminController@index');

});
