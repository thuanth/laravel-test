<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function viewContact(Request $request) {
        echo $request->method();
        return view('contact.index');
    }

    public function postContact(Request $request) {
        echo url()->current()."<br/>";
        echo url()->full()."<br/>";
        echo url()->previous()."<br/>";
        echo $request->method();
        return view('contact.index');
    }
}
