<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Contracts\EventPusher;

class PhotoController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(EventPusher $pusher)
    {
        echo url()->current()."<br/>";
        /* use bind */
        $bind = app('printAge');
        $bind->setUsername('khanhdq119@gmail.com');
        echo $bind->getUsername()."<br/>";
        $bind1 = app('printAge');
        echo $bind1->getUsername()."<br/>";
        /* use bind */

        /* use singleton */
        $singleton = app('printMyName');
        $singleton->setUsername('khanhdq119@rocketmail.com');
        echo $singleton->getUsername()."<br/>";
        $singleton1 = app('printMyName');
        echo $singleton1->getUsername()."<br/>";
        /* use singleton */
        echo $pusher->cronJobSendMail();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $jobs = ['teacher','doctor','engineer','worker'];
        $date = time();
        return view('photo.index',['jobs'=>$jobs,'date'=>$date]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //$request->flash();
        return redirect()->back()->with([
            'status' => 'test 11111111111','abc'=>'aaaaa',
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        echo route('photo.show',['id'=>1])."<br/>";
        echo route('photo.edit',['id'=>1])."<br/>";
        echo route('photo.update',['id'=>1])."<br/>";
        echo route('photo.destroy',['id'=>1])."<br/>";
    }

    /**
     * Show the form for editing the specified resource.
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        //
        echo $id."<br/>";
        echo $request->cookie('username');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        echo "Email 1: ".$request->input('email.0')."<br/>";
        echo "Email 2: ".$request->input('email.1')."<br/>";
        echo "Get all: ".var_dump($request->all())."<br/>";
        //
        echo $request->method()."<br/>";
        if ($request->isMethod('post')) {
            echo "use request->isMethod('post')"."<br/>";
        }
        echo $request->url()."<br/>";
        echo $request->fullUrl()."<br/>";
        if ($request->is('photo')) {
            echo "ok"."<br/>";
        } else {
            echo "fail"."<br/>";
        }
        echo $request->fullUrlWithQuery(['bar' => 'baz'])."<br/>";
        echo url()->current()."<br/>";
        echo $id."<br/>";
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        echo "destroy ".$id."<br/>";
    }
}
