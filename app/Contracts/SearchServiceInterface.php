<?php

namespace App\Contracts;

interface SearchServiceInterface {
    public function getItems($keyword);
}