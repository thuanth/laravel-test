<?php

namespace App\Contracts;


interface EventPusher {
    public function sentMailCustomer($email);
    public function cronJobSendMail();
}