@extends('layouts/master')
@section('sidebar')
    @parent
    <p>This is appended to the master sidebar.</p>
@endsection
@section('content')
    <?php var_dump($errors->all()); ?>
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    <form id="form_login_email" method="post" action="{{route('photo.store')}}">
        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
        <div class="form-group">
            <label for="username">Username</label>
            <input type="text" autofocus="" class="form-control" value="{{ old('username') }}" name="username"
                   id="username" tabindex="2" autocapitalize="off">
        </div>
        <div class="form-group">
            <label for="username">Email 1</label>
            <input type="text" autofocus="" class="form-control" value="" name="email[]"
                   id="username" tabindex="2" autocapitalize="off">
        </div>
        <div class="form-group">
            <label for="username">Email 2</label>
            <input type="text" autofocus="" class="form-control" value="" name="email[]"
                   id="username" tabindex="2" autocapitalize="off">
        </div>
        <div class="error errorMessage-text js-loginError">&nbsp;</div>
        <div>
            <button class="submit btn btn-warning btn-raised" type="submit" id="submit_login_email" tabindex="4">Log
                in
            </button>
        </div>
    </form>
    {{-- This comment will not be present in the rendered HTML --}}
    //@each('photo.view', $jobs, 'job','photo.empty')
    <p>@datetime($date)</p>
    @inject('metrics', 'App\Services\MetricsService')
    <div>
        Monthly Revenue: {{ $metrics->monthlyRevenue() }}.
    </div>
@endsection
@push('scripts')
<script type="text/javascript">
    alert("test push script");
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
@endpush
