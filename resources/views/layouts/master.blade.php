<html>
<head>
    <title>App Name</title>
    @stack('scripts')
</head>
<body>
    Hello, @{{ name }}.
    @section('sidebar')
        This is the master sidebar.
    @show
    <h3>
        @section('sectionShow')
            This is the section show.
        @show
    </h3>
    <div class="container">
        @yield('content')
    </div>
</body>
</html>