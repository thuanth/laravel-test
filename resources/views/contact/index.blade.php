<!DOCTYPE html>
<html>
<head>
    <title>Laravel</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body>
    <form id="form_login_email" method="post" action="{{route('post_contact')}}">
        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
        <div class="form-group">
            <label for="username">Email</label>
            <input type="email" autofocus="" class="form-control" value="" name="username"
                   id="username" tabindex="2" autocapitalize="off">
        </div>

        <div class="form-group">
            <label for="password">Password</label>
            <input type="password" class="form-control" value="" name="password" id="password"
                   tabindex="3">
        </div>
        <div class="error errorMessage-text js-loginError">&nbsp;</div>
        <div>
            <button class="submit btn btn-warning btn-raised" type="submit" id="submit_login_email" tabindex="4">Log
                in
            </button>
        </div>
    </form>
</body>
